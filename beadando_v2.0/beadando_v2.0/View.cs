﻿namespace Beadando_v2._0
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    public class View : FrameworkElement
    {
        ImageBrush background;
        ImageBrush vader;        
        ImageBrush jatekmezo;
        ImageBrush empHatter;
        ImageBrush wookiee;
        ImageBrush gyozelem;
        Logic VM;

        public void initialize(Logic VM)
        {
            this.VM = VM;
            background = new ImageBrush(
               new BitmapImage(new Uri
               ("bg.jpg", UriKind.RelativeOrAbsolute)
               ));
            jatekmezo = new ImageBrush(
                new BitmapImage(new Uri
                ("Kashyyyk.jpg", UriKind.RelativeOrAbsolute)
                ));
            
            vader = new ImageBrush(
                new BitmapImage(new Uri
                ("vader trans.png", UriKind.RelativeOrAbsolute)
                ));
            empHatter = new ImageBrush(new BitmapImage(new Uri("Darth_Vader.png", UriKind.RelativeOrAbsolute)));
            wookiee = new ImageBrush(new BitmapImage(new Uri("wookiee.png", UriKind.RelativeOrAbsolute)));
            gyozelem = new ImageBrush(new BitmapImage(new Uri("won.jpg", UriKind.RelativeOrAbsolute)));
        }

        protected override void OnRender(DrawingContext drawingcontext)
        {
            if (VM != null && !VM.Vege())
            {
                drawingcontext.DrawRectangle(
                    background,
                    null,
                    new Rect(0, 0, this.ActualWidth, this.ActualHeight)
                    );
                drawingcontext.DrawRectangle(
                    jatekmezo,
                    new Pen(Brushes.Black, 5),
                    VM.Map
                    );
                
                if (VM.TestForCombine.Count != 0)
                {
                    drawingcontext.DrawGeometry(empHatter, null, VM.TestForCombine[0]);
                }
                
                //vader mukodo
                drawingcontext.DrawRectangle(
                    vader,
                    null,
                   new Rect(VM.Vader.Center.X-35, VM.Vader.Center.Y-35, 70, 70)
                    );
                foreach (Ellenfel item in VM.Ellenfelek)
                {
                    drawingcontext.DrawRectangle(
                    wookiee,
                    null,
                    new Rect(item.Center.X - 35, item.Center.Y - 35, 70, 70)
                    );
                }
                
                drawingcontext.DrawText(new FormattedText(VM.Vader.Allapot.ToString() + ", " + VM.Vader.Center.ToString() + ", " + VM.Map.Width + ":" + VM.Map.Height
                    + ", " + VM.Map.Width + ", " + VM. Map.Height + ", " + VM.Fontos_pontok.Count, System.Globalization.CultureInfo.CurrentCulture,
                    FlowDirection.LeftToRight, new Typeface(
                        new FontFamily("Arial"), FontStyles.Normal, FontWeights.Bold,
                        FontStretches.Normal), 14, Brushes.White), new Point(VM.Map.Left + 1000, VM.Map.Top + 50)
                        );
                
                if(VM.Vader.Allapot == state.isActive)
                {
                    foreach (Rect item in VM.Aktualisut)
                    {
                        drawingcontext.DrawRectangle(Brushes.Black, null, item);
                    }
                }

                //képeszség:
                drawingcontext.DrawText(new FormattedText("Skill Reset in:" + VM.Reset.ToString(), System.Globalization.CultureInfo.CurrentCulture,
                        FlowDirection.LeftToRight, new Typeface(
                            new FontFamily("Arial"), FontStyles.Normal, FontWeights.Bold,
                            FontStretches.Normal), 30, Brushes.White), new Point(150, 450));
                drawingcontext.DrawRectangle(
                    Brushes.Black,
                    null,
                   new Rect(200,500 ,20, VM.Reset == 0 ? 200 : 200/VM.Reset)
                    );
                drawingcontext.DrawText(new FormattedText("Skill in use:" + VM.KepessegOn.ToString(), System.Globalization.CultureInfo.CurrentCulture,
                        FlowDirection.LeftToRight, new Typeface(
                            new FontFamily("Arial"), FontStyles.Normal, FontWeights.Bold,
                            FontStretches.Normal), 30, Brushes.White), new Point(100, 800));
            
            }
            else
            {
                drawingcontext.DrawRectangle(
                    gyozelem,
                    null,
                    new Rect(0, 0, this.ActualWidth, this.ActualHeight)
                    );
            }
        }
    }
}
