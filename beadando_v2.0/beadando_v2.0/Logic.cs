﻿namespace Beadando_v2._0
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Threading;
    enum state
    {
        isActive, notActive
    }
    public class Logic
    {
        Size jatekmeret;
        Rect map;
        VaderModel vader;
        DispatcherTimer dt;       
        int timer, passed_time, slowtimer ,slow_passed_time, absorbtimer,absorb_passed_time, reset;            
        List<Point> fontos_pontok;       
        List<Rect> aktualisut;
        List<StreamGeometry> streamGeometry;        
        Point elhagyas;
        Point erkezes;        
        Key lastbutton;
        bool lefutott = true;
        Geometry combined;
        List<Geometry> testForCombine; 
        List<Ellenfel> ellenfelek;
        string kepessegOn;
        int speed;     

        public VaderModel Vader
        {
            get
            {
                return vader;
            }

            set
            {
                vader = value;
            }
        }

        public Rect Map
        {
            get
            {
                return map;
            }

            set
            {
                map = value;
            }
        }       

        public List<Rect> Aktualisut
        {
            get
            {
                return aktualisut;
            }

            set
            {
                aktualisut = value;
            }
        }

        public List<StreamGeometry> StreamGeometry
        {
            get
            {
                return streamGeometry;
            }

            set
            {
                streamGeometry = value;
            }
        }

        public List<Point> Fontos_pontok
        {
            get
            {
                return fontos_pontok;
            }

            set
            {
                fontos_pontok = value;
            }
        }

        public Geometry Combined
        {
            get
            {
                return combined;
            }

            set
            {
                combined = value;
            }
        }

        public List<Geometry> TestForCombine //test
        {
            get
            {
                return testForCombine;
            }

            set
            {
                testForCombine = value;
            }
        }

        public List<Ellenfel> Ellenfelek
        {
            get
            {
                return ellenfelek;
            }

            set
            {
                ellenfelek = value;
            }
        }

        public int Speed
        {
            get
            {
                return speed;
            }

            set
            {
                speed = value;
            }
        }

        public int Timer
        {
            get
            {
                return timer;
            }

            set
            {
                timer = value;
            }
        }

        public int Passed_time
        {
            get
            {
                return passed_time;
            }

            set
            {
                passed_time = value;
            }
        }

        public int Slowtimer
        {
            get
            {
                return slowtimer;
            }

            set
            {
                slowtimer = value;
            }
        }

        public int Slow_passed_time
        {
            get
            {
                return slow_passed_time;
            }

            set
            {
                slow_passed_time = value;
            }
        }

        public int Absorbtimer
        {
            get
            {
                return absorbtimer;
            }

            set
            {
                absorbtimer = value;
            }
        }

        public int Absorb_passed_time
        {
            get
            {
                return absorb_passed_time;
            }

            set
            {
                absorb_passed_time = value;
            }
        }

        public int Reset
        {
            get
            {
                return reset;
            }

            set
            {
                reset = value;
            }
        }

        public string KepessegOn
        {
            get
            {
                return kepessegOn;
            }

            set
            {
                kepessegOn = value;
            }
        }

        public Logic(Size jatekmeret)
        {
            this.jatekmeret = jatekmeret;
            this.Map = new Rect(20 * jatekmeret.Width / 100,
                10 * jatekmeret.Height / 100,
                75 * jatekmeret.Width / 100,
                85 * jatekmeret.Height / 100);
            this.Vader = new VaderModel(Map);
            dt = new DispatcherTimer();
            dt.Interval = TimeSpan.FromSeconds(1);
            dt.Tick += Dt_Tick;
            dt.Start();            
            Fontos_pontok = new List<Point>();
            Fontos_pontok.Add(vader.Center);            
            Aktualisut = new List<Rect>();
            erkezes = vader.Center;            
            StreamGeometry = new List<StreamGeometry>();
            lastbutton = Key.K;
            combined = new StreamGeometry();
            TestForCombine = new List<Geometry>(); //test
            Ellenfelek = new List<Ellenfel>();
            Speed = 1;           
            KepessegOn = "";
            
            for (int i = 0; i < 10; i++)
            {
                ellenfelek.Add(new Ellenfel(Map, new Vector(3, 3)));//ellenfelek sebessege szintek eseten
            }
            
        }
        public int SetNmySpeed()
        {
            
            if(Speed == 1 )
            {
                Speed = 2;
                for (int i = 0; i < ellenfelek.Count; i++)
                {
                    ellenfelek[i].Sebesseg = new Vector(ellenfelek[i].Sebesseg.X * 1.5, ellenfelek[i].Sebesseg.Y * 1.5);
                }
                return 0;
            }
            if (Speed == 2)
            {
                Speed = 3;
                for (int i = 0; i < ellenfelek.Count; i++)
                {
                    ellenfelek[i].Sebesseg = new Vector(ellenfelek[i].Sebesseg.X * 2, ellenfelek[i].Sebesseg.Y * 2);
                }
                return 0;
            }
            if (Speed == 3)
            {
                Speed = 1;
                for (int i = 0; i < ellenfelek.Count; i++)
                {
                    ellenfelek[i].Sebesseg = new Vector(3, 3);
                }
                return 0;
            }
            return 0;
        }
        public int SetNmySpeed(double ertek)
        {            
                for (int i = 0; i < ellenfelek.Count; i++)
                {
                    ellenfelek[i].Sebesseg = new Vector(ellenfelek[i].Sebesseg.X * ertek, ellenfelek[i].Sebesseg.Y * ertek);
                }
                return 0;
                 
            
        }

        private void Dt_Tick(object sender, EventArgs e)
        {
            
            for (int i = 0; i < ellenfelek.Count; i++)
            {
                if(ellenfelek[i].Bekeritve(TestForCombine))
                {
                    ellenfelek.Remove(ellenfelek[i]);
                    vader.Killed++;
                    continue;                    
                }
                ellenfelek[i].Mozog(TestForCombine);
                if (!Vader.AbsorbOn)
                {
                    if (ellenfelek[i].CollosionDetectUniv(aktualisut))
                    {
                        MessageBox.Show("Vesztettel");
                        Application.Current.Shutdown();
                    }
                }
            }            
            Timer += 1;
            Slowtimer += 1;
            Absorbtimer += 1;
            if (vader.SpeedOn)
            {
                
                if (Timer - Passed_time >= 5)
                {
                    ResetAttribSpeed();
                    kepessegOn = "";
                }
            }
            if (vader.SlowOn)
            {
                if (Slowtimer - Slow_passed_time >=5)
                {
                    ResetAttribSlow();
                    kepessegOn = "";
                }
            }
            if (vader.AbsorbOn)
            {
                if (Absorbtimer - Absorb_passed_time >=3)
                {
                    ResetAttribAbsorb();
                    kepessegOn = "";
                }
            }
            if (Reset > 0)
                Reset -= 1; 
            
        }

        public void Billentyulenyomas(Key e)
        {
            Elkerit();
            if (e != lastbutton)
            {
                Fontos_pontok.Add(new Point(vader.Center.X, vader.Center.Y));
            }
            lastbutton = e;
            if (e == Key.Left)
            {
                Vader.Mozgas(-1, 0);
            }
            else if (e == Key.Right)
            {
                Vader.Mozgas(1, 0);
            }
            else if (e == Key.Down)
            {
                Vader.Mozgas(0, 1);
            }
            else if (e == Key.Up)
            {
                Vader.Mozgas(0, -1);
            }
            else if (e == Key.E)
            {
                Passed_time = Timer;                
                if (vader.SlowOn != true && vader.SpeedOn != true && vader.AbsorbOn != true && !inReset())
                {
                    vader.SpeedOn = true;
                    vader.Force_Speed(2, 2);
                    KepessegOn = "Speed";
                }
                Reset = 7;
            }
            else if (e == Key.Q)
            {
                Slow_passed_time = Slowtimer;
                
                if (vader.SlowOn != true && vader.SpeedOn != true && vader.AbsorbOn != true && !inReset())
                {
                    vader.SlowOn = true;
                    SetNmySpeed(0.5);
                    KepessegOn = "Slow";
                }
                Reset = 7;
            }
            else if (e == Key.R)
            {
                Absorb_passed_time = Absorbtimer;
                
                if (vader.AbsorbOn != true && vader.SpeedOn != true && vader.SlowOn != true && !inReset())
                {
                    vader.AbsorbOn = true;
                    KepessegOn = "Absorb";

                }
                Reset = 15;
            }


        }

        private bool inReset()
        {
            if (Reset > 0)
                return true;
            return false;
        }

        private void ResetAttribSpeed()
        {

            vader.SpeedOn = false;
            vader.Force_Speed(0.5, 0.5);
        }
        private void ResetAttribSlow()
        {

            vader.SlowOn = false;
            SetNmySpeed(2);
        }
        private void ResetAttribAbsorb()
        {
            vader.AbsorbOn = false;
        }

        public void Elkerit()
        {                     
                        
            if (vader.Allapot == state.isActive)
            {
                lefutott = false;
                if (vader.Irany == irany.balra)
                {
                    erkezes = new Point(vader.Center.X-5,vader.Center.Y);
                }
                else if(vader.Irany == irany.fel)
                {
                    erkezes = new Point(vader.Center.X, vader.Center.Y-5);
                }
                else if (vader.Irany == irany.jobbra)
                {
                    erkezes = new Point(vader.Center.X+5, vader.Center.Y);
                }
                else if (vader.Irany == irany.le)
                {
                    erkezes = new Point(vader.Center.X, vader.Center.Y + 5);
                }

                if (fontos_pontok.Count == 0)
                {
                    fontos_pontok.Add(elhagyas);
                }
                if (vader.SpeedOn && (vader.Irany == irany.balra || vader.Irany == irany.jobbra))
                {
                    Aktualisut.Add(new Rect(vader.Center.X, vader.Center.Y, 10, 5));
                }
                else if (vader.SpeedOn && (vader.Irany == irany.le || vader.Irany == irany.fel))
                {
                    Aktualisut.Add(new Rect(vader.Center.X, vader.Center.Y, 5, 10));
                }
                else 
                {
                    Aktualisut.Add(new Rect(vader.Center.X, vader.Center.Y, 5, 5));
                                       
                }

            }
            else
            {               
                
                Aktualisut.Clear();
                if (!lefutott)
                {
                    test();
                }
                
                elhagyas = vader.Center;
                Fontos_pontok.Clear();
                if (Vege())
                {
                    MessageBox.Show("nyertel, probald ki a jatekot magassabb sebességi fokozaton");

                }

            }

        }
        public bool Vege()
        {if (TestForCombine.Count != 0)
            {
                if (TestForCombine[0].GetArea() >= (map.Width * map.Height * 0.9))
                    return true;
            }
            return false;
            
        }
        
        public void test()
        {
            StreamGeometry current = new StreamGeometry();

            using (StreamGeometryContext geometryContext = current.Open())
            {
                geometryContext.BeginFigure(new Point(elhagyas.X, elhagyas.Y), true, true);
               
                Fontos_pontok.Add(erkezes);

                bool vertikalisAt = (vader.Center.Y <= map.Top + 10 && elhagyas.Y >= map.Bottom - 10) || //felfele
                    (vader.Center.Y >= map.Bottom -10 && elhagyas.Y <= map.Top +10); //lefele
                bool horizontalisAt = (vader.Center.X <= map.Left + 5 && elhagyas.X >= map.Right - 5) || //balra;
                    (vader.Center.X >= map.Right - 5 && elhagyas.X <= map.Left + 5); // jobbra
                if (fontos_pontok.Count == 2 || vertikalisAt || horizontalisAt)
                {
                    if ((vertikalisAt && vader.Center.X - map.X < map.Width / 2) && vader.Irany == irany.le)
                    {
                        fontos_pontok.Add(new Point(map.Left + 5, map.Bottom - 5));
                        fontos_pontok.Add(new Point(map.Left + 5, map.Top + 5));
                    }
                    else if ((vertikalisAt && vader.Center.X - map.X < map.Width / 2) && vader.Irany == irany.fel)
                    {
                        fontos_pontok.Add(new Point(map.Left + 5, map.Top + 5));
                        fontos_pontok.Add(new Point(map.Left + 5, map.Bottom - 5));
                    }
                    else if ((vertikalisAt && vader.Center.X - map.X > map.Width / 2) && vader.Irany == irany.le)
                    {
                        fontos_pontok.Add(new Point(map.Right - 5, map.Bottom - 5));
                        fontos_pontok.Add(new Point(map.Right - 5, map.Top + 5));
                    }
                    else if ((vertikalisAt && vader.Center.X - map.X > map.Width / 2) && vader.Irany == irany.fel)
                    {
                        fontos_pontok.Add(new Point(map.Right - 5, map.Top + 5));
                        fontos_pontok.Add(new Point(map.Right - 5, map.Bottom - 5));
                    }
                    else if ((horizontalisAt && vader.Center.Y - map.Y < map.Height / 2) && vader.Irany == irany.balra)
                    {
                        fontos_pontok.Add(new Point(map.Left + 5, map.Top + 5));
                        fontos_pontok.Add(new Point(map.Right - 5, map.Top + 5));
                    }
                    else if ((horizontalisAt && vader.Center.Y - map.Y < map.Height / 2) && vader.Irany == irany.jobbra)
                    {
                        fontos_pontok.Add(new Point(map.Right - 5, map.Top + 5));
                        fontos_pontok.Add(new Point(map.Left + 5, map.Top + 5));
                    }
                    else if ((horizontalisAt && vader.Center.Y - map.Y > map.Height / 2) && vader.Irany == irany.balra)
                    {
                        fontos_pontok.Add(new Point(map.Left + 5, map.Bottom - 5));
                        fontos_pontok.Add(new Point(map.Right - 5, map.Bottom - 5));
                    }
                    else
                    {
                        fontos_pontok.Add(new Point(map.Right - 5, map.Bottom - 5));
                        fontos_pontok.Add(new Point(map.Left + 5, map.Bottom - 5));
                    }
                }
                if (fontos_pontok.Count >= 3 && !vertikalisAt && !horizontalisAt)
                {
                    if ((vader.Center.X <= elhagyas.X && vader.Center.Y >= elhagyas.Y && vader.Irany == irany.balra) ||
                        (vader.Center.X >= elhagyas.X && vader.Center.Y <= elhagyas.Y && vader.Irany == irany.fel))
                    {
                        Fontos_pontok.Add(new Point(map.Left + 5, map.Top + 5));
                    }
                    else if ((vader.Center.X >= elhagyas.X && vader.Center.Y >= elhagyas.Y && vader.Irany == irany.jobbra) ||
                        (vader.Center.X <= elhagyas.X && vader.Center.Y <= elhagyas.Y && vader.Irany == irany.fel))
                    {
                        Fontos_pontok.Add(new Point(map.Right - 5, map.Top + 5));
                    }
                    else if ((vader.Center.X <= elhagyas.X && vader.Center.Y >= elhagyas.Y && vader.Irany == irany.le) ||
                        (vader.Center.X >= elhagyas.X && vader.Center.Y <= elhagyas.Y && vader.Irany == irany.jobbra))
                    {
                        Fontos_pontok.Add(new Point(map.Right - 5, map.Bottom - 5));
                    }
                    else if ((vader.Center.X >= elhagyas.X && vader.Center.Y >= elhagyas.Y && vader.Irany == irany.le) || // 
                         (vader.Center.X <= elhagyas.X && vader.Center.Y <= elhagyas.Y && vader.Irany == irany.balra)) //
                    {
                        Fontos_pontok.Add(new Point(map.Left + 5, map.Bottom - 5));
                    }
                }
               


                geometryContext.PolyLineTo(Fontos_pontok, true, true);
                lefutott = true;
            }

            if (testForCombine.Count != 0)
            {
                Geometry combined = Geometry.Combine(current, testForCombine[0], GeometryCombineMode.Union, null);
                testForCombine.Clear();
                testForCombine.Add(combined);                
            }
            else
            {
                testForCombine.Add(current);
            }
            
            
        }
       
       
    }
    
}

