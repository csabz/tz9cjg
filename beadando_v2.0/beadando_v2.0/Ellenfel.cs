﻿namespace Beadando_v2._0
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;

    public class Ellenfel
    {
        Point center;
        Rect palya;
        Vector sebesseg;
        static Random r = new Random();

        public Point Center
        {
            get
            {
                return center;
            }

            set
            {
                center = value;
            }
        }

        public Vector Sebesseg
        {
            get
            {
                return sebesseg;
            }

            set
            {
                sebesseg = value;
            }
        }

        public Ellenfel(Rect palya,Vector sebesseg)
        {
            this.Center = new Point(r.Next((int)palya.Left, (int)palya.Right), r.Next((int)palya.Top, (int)palya.Bottom));
            this.palya = palya;
            this.Sebesseg = new Vector(2*sebesseg.X, 2*sebesseg.Y);
            
            
        }
        int Randomizalo(int min, int max)
        {
            int vel = 0;
            do
            {
                vel = r.Next(min, max);
            } while (vel == 0);
            return vel;
        }


        public void Mozog(List<Geometry> elkeritett)
        {
            bool lephet = false;
            Point newpoint;
            int max = 0;
            while (max >= 3 || !lephet)
            {
                newpoint= new Point(Randomizalo(-1, 2) * Sebesseg.X + Center.X, Randomizalo(-1, 2) * Sebesseg.Y + Center.Y);
                if (newpoint.X > palya.Left && newpoint.X < palya.Right &&
                    newpoint.Y > palya.Top && newpoint.Y < palya.Bottom &&
                    !CollosionDetect(elkeritett, newpoint))
                {
                    Center = newpoint;
                    lephet = true;
                }
                max++;
            }
            max = 0;
            

        }

        public bool CollosionDetect(List<Geometry> elkeritett, Point cent) // fallal
        {
            if (elkeritett.Count != 0)
            {
                return Geometry.Combine(new RectangleGeometry(new Rect(cent.X - 35, cent.Y - 35, 70, 70)),
                    elkeritett[0],
                    GeometryCombineMode.Intersect,
                    null).GetArea() > 0;
            }
            else
                return false;
        }

        public bool Bekeritve(List<Geometry> elkeritett)
        {
            if (elkeritett.Count != 0)
            {
                return Geometry.Combine(new RectangleGeometry(new Rect(this.center.X - 35, this.center.Y - 35, 1, 1)),
                elkeritett[0],
                GeometryCombineMode.Union,
                null).GetArea() == elkeritett[0].GetArea();
            }
            else
                return false;
        }

        public bool CollosionDetectUniv(List<Rect> obstacles)
        {
            if (obstacles.Count != 0)
            {
                foreach (Rect item in obstacles)
                {
                    if (Geometry.Combine(new RectangleGeometry(new Rect(this.center.X - 35, this.center.Y - 35, 70, 70)),
                    new RectangleGeometry(item),
                    GeometryCombineMode.Intersect,
                    null).GetArea() > 0)
                    {
                        return true;
                    }
                                           
                }
                return false;
            }
            else
                return false;
        }

        
         
    }
}
