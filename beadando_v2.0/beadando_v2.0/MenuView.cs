﻿namespace Beadando_v2._0
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    class MenuView : FrameworkElement
    {
        Logic VM;
        ImageBrush menuImage;
        public void initialize(Logic VM)
        {
            this.VM = VM;
            menuImage = new ImageBrush(
               new BitmapImage(new Uri
               ("jkmenu.png", UriKind.RelativeOrAbsolute)
               ));

        }

        protected override void OnRender(DrawingContext drawingcontext)
        {
            if (VM != null)
            {
                drawingcontext.DrawRectangle(
                        menuImage,
                        null,
                        new Rect(0, 0, this.ActualWidth, this.ActualHeight)
                        );

                drawingcontext.DrawRectangle(Brushes.Transparent, null, new Rect(this.ActualWidth / 8, this.ActualHeight / 8, 170, 50));
                drawingcontext.DrawRectangle(Brushes.Transparent, null, new Rect(this.ActualWidth / 8, 2 * this.ActualHeight / 8, 145, 50));
                drawingcontext.DrawRectangle(Brushes.Transparent, null, new Rect(2 * this.ActualWidth / 8, 2 * this.ActualHeight / 8, 20, 50));
                drawingcontext.DrawRectangle(Brushes.Transparent, null, new Rect(this.ActualWidth / 8, 3 * this.ActualHeight / 8, 160, 50));

                drawingcontext.DrawText(new FormattedText("New Game", System.Globalization.CultureInfo.CurrentCulture,
                        FlowDirection.LeftToRight, new Typeface(
                            new FontFamily("Arial"), FontStyles.Normal, FontWeights.Bold,
                            FontStretches.Normal), 30, Brushes.White), new Point(this.ActualWidth / 8 + 5, this.ActualHeight / 8 + 5));


                drawingcontext.DrawText(new FormattedText("Sebesség:", System.Globalization.CultureInfo.CurrentCulture,
                       FlowDirection.LeftToRight, new Typeface(
                           new FontFamily("Arial"), FontStyles.Normal, FontWeights.Bold,
                           FontStretches.Normal), 30, Brushes.White), new Point(this.ActualWidth / 8 + 5, 2 * this.ActualHeight / 8 + 5));

                drawingcontext.DrawText(new FormattedText(VM.Speed.ToString(), System.Globalization.CultureInfo.CurrentCulture,
                        FlowDirection.LeftToRight, new Typeface(
                            new FontFamily("Arial"), FontStyles.Normal, FontWeights.Bold,
                            FontStretches.Normal), 30, Brushes.White), new Point(2 * this.ActualWidth / 8 + 5, 2 * this.ActualHeight / 8 + 5));
                drawingcontext.DrawText(new FormattedText("Exit Game", System.Globalization.CultureInfo.CurrentCulture,
                       FlowDirection.LeftToRight, new Typeface(
                           new FontFamily("Arial"), FontStyles.Normal, FontWeights.Bold,
                           FontStretches.Normal), 30, Brushes.White), new Point(this.ActualWidth / 8 + 5, 3 * this.ActualHeight / 8 + 5));
            }
        }
    }
}