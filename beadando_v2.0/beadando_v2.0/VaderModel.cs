﻿namespace Beadando_v2._0
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    enum irany
    {
        balra,jobbra,fel,le
    }
    public class VaderModel
    {
        Point center;
        Rect palya;
        state allapot;
        Vector sebesseg;
        bool speedOn;
        bool slowOn;
        bool absorbOn;
        irany irany;
        int killed;

        public Point Center
        {
            get
            {
                return center;
            }

            set
            {
                center = value;
            }
        }

        internal state Allapot
        {
            get
            {
                return allapot;
            }

            set
            {
                allapot = value;
            }
        }

        public Vector Sebesseg
        {
            get
            {
                return sebesseg;
            }

            set
            {
                sebesseg = value;
            }
        }

        public bool SpeedOn
        {
            get
            {
                return speedOn;
            }

            set
            {
                speedOn = value;
            }
        }

        internal irany Irany
        {
            get
            {
                return irany;
            }

            set
            {
                irany = value;
            }
        }

        public int Killed
        {
            get
            {
                return killed;
            }

            set
            {
                killed = value;
            }
        }

        public bool SlowOn
        {
            get
            {
                return slowOn;
            }

            set
            {
                slowOn = value;
            }
        }

        public bool AbsorbOn
        {
            get
            {
                return absorbOn;
            }

            set
            {
                absorbOn = value;
            }
        }

        public VaderModel(Rect palya)
        {
            this.palya = palya;
            Center = new Point(palya.Left+5, palya.Top+5);
            Allapot = state.notActive;
            Sebesseg = new Vector(5, 5);
            SpeedOn = false;
            slowOn = false;
            absorbOn = false;
            killed = 0; 
        }

        public void Mozgas(double dx, double dy)
        {
            if (dx == 1)
            {
                irany = irany.jobbra;
            }
            else if (dx == -1)
            {
                irany = irany.balra;
            }
            else if (dy == 1)
            {
                irany = irany.le;
            }
            else
            {
                irany = irany.fel;
            }
            Point newpoint = new Point(dx*sebesseg.X + center.X, dy*sebesseg.Y + center.Y);
            if(newpoint.X > palya.Left && newpoint.X < palya.Right &&
                newpoint.Y > palya.Top && newpoint.Y < palya.Bottom)
            {
                center = newpoint;
                if(center.X >= palya.Left+10 && center.X <= palya.Right-10 &&
                center.Y >= palya.Top+10 && center.Y <= palya.Bottom-10)
                {
                    allapot = state.isActive;
                }
                else
                {
                    allapot = state.notActive;
                }
            }
        }

        public void Force_Speed(double dx, double dy)
        {
            sebesseg.X *= dx;
            sebesseg.Y *= dy;
        }
        



    }

    
}
