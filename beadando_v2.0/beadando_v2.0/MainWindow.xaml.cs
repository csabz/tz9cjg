﻿namespace Beadando_v2._0
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;
    using System.Windows.Threading;

    //Németh Csaba, TZ9CJG

    public partial class MainWindow : Window
    {
        Logic VM;
        DispatcherTimer dt;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            VM = new Logic(new Size(jatekter.ActualWidth, jatekter.ActualHeight));
            menu.initialize(VM);
            menu.InvalidateVisual();
            this.MouseLeftButtonDown += MainWindow_MouseLeftButtonDown;
            

        }

        private void MainWindow_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Rect katt = new Rect(e.GetPosition(menu).X, e.GetPosition(menu).Y, 1, 1);
            if (katt.IntersectsWith(new Rect(this.ActualWidth / 8, this.ActualHeight / 8, 170, 50)))
            {
               
                
                jatekter.initialize(VM);
                dt = new DispatcherTimer();
                dt.Interval = TimeSpan.FromSeconds(1);
                dt.Tick += Dt_Tick;
                dt.Start();
                this.KeyDown += MainWindow_KeyDown;
                jatekter.InvalidateVisual();
                this.MouseLeftButtonDown -= MainWindow_MouseLeftButtonDown;
            }
            else if(katt.IntersectsWith(new Rect(this.ActualWidth / 8, 2 * this.ActualHeight / 8, 135, 50)))
            {
                VM.SetNmySpeed();
                menu.InvalidateVisual();
            }
            else if (katt.IntersectsWith(new Rect(this.ActualWidth / 8, 3 * this.ActualHeight / 8, 160, 50)))
            {
                Application.Current.Shutdown();
            }
        }

        private void Dt_Tick(object sender, EventArgs e)
        {
            jatekter.InvalidateVisual();
        }

        private void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            VM.Billentyulenyomas(e.Key);
            jatekter.InvalidateVisual();
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            VM = new Logic(new Size(jatekter.ActualWidth, jatekter.ActualHeight));
            jatekter.initialize(VM);
        }
    }
}
